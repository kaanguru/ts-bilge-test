# NativeScript-Vue Application

> A native application built with NativeScript-Vue and Typescript

## Usage

``` bash
# Install dependencies
yarn install

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run --no-hmr

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production

```
